
import java.util.*;

public abstract class Human {
    // field
    protected String name, surname;
    protected int year, iq;
    protected Family family;
    protected DayOfWeek monday = DayOfWeek.MONDAY;;
    protected DayOfWeek tuesday = DayOfWeek.TUESDAY;;
    protected DayOfWeek wednesday = DayOfWeek.WEDNESDAY;;
    protected DayOfWeek thursday = DayOfWeek.THURSDAY;;
    protected DayOfWeek friday = DayOfWeek.FRIDAY;;
    protected DayOfWeek saturday = DayOfWeek.SATURDAY;;
    protected DayOfWeek sunday = DayOfWeek.SUNDAY;;

    // constructors
    public Human() {
    }
    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    // methods
    public void describePet() {
        System.out.print("I have a " + family.getPet().getSpecies() +
                ", he is " + family.getPet().getAge() + " years old, he is ");
        System.out.println((family.getPet().getTrickLevel() > 50) ? "very sly." : "almost not sly.");
    }

    // Override methods
    @Override
    public String toString() {
        return "Human{" +
                "name = '" + name + '\'' +
                ", surname = '" + surname + '\'' +
                ", year = " + year +
                ", iq = " + iq +
                ", schedule = " + monday + " " + monday.get() + " "
                + tuesday + " " + tuesday.get() + " "
                + wednesday + " " + wednesday.get() + " "
                + thursday + " " + thursday.get() + " "
                + friday + " " + friday.get() + " "
                + saturday + " " + saturday.get() + " "
                + sunday + " " + sunday.get() + "}";
    }

    // setters
    public void setName(String name) {
        this.name = name;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.year = year;
    }
    public void setIq(int iq) {
        this.iq = iq;
    }
    public void setFamily(Family family) {
        this.family = family;
    }
    public void setSchedule(String schedule, String doing){
        switch(schedule.toUpperCase()){
            case "MONDAY":
                this.monday.set(doing);
                break;
            case "TUESDAY":
                this.tuesday.set(doing);
                break;
            case "WEDNESDAY":
                this.wednesday.set(doing);
                break;
            case "THURSDAY":
                this.thursday.set(doing);
                break;
            case "FRIDAY":
                this.friday.set(doing);
                break;
            case "SATURDAY":
                this.saturday.set(doing);
                break;
            case "SUNDAY":
                this.sunday.set(doing);
                break;
            default:
                System.out.println("Please write correct days of the week");
                break;
        }
    }

    // getters
    public String getName() {
        return name;
    }
    public String getSurname() {
        return surname;
    }
    public int getYear() {
        return year;
    }
    public int getIq() {
        return iq;
    }
    public Family getFamily() {
        return family;
    }
    public String getSchedule() {
        return monday + " " + monday.get() + " "
                + tuesday + " " + tuesday.get() + " "
                + wednesday + " " + wednesday.get() + " "
                + thursday + " " + thursday.get() + " "
                + friday + " " + friday.get() + " "
                + saturday + " " + saturday.get() + " "
                + sunday + " " + sunday.get();
    }
}

final class Man extends Human {
    // constructors
    public Man() {
    }
    public Man(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Man(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    // methods
    public void repairCar() {
        System.out.println("It's time to repair car.");
    }
    public void greetPet() {
        System.out.println("Hello, " + family.getPet().getNickname() + '.');
    }
}
final class Woman extends Human {
    // constructors
    public Woman() {
    }
    public Woman(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }
    public Woman(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    // methods
    public void makeUp() {
        System.out.println("It's time to go to the beautician.");
    }
    public void greetPet() {
        System.out.println("Hello, my sweeties " + family.getPet().getNickname() + '.');
    }
}

