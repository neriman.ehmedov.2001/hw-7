import java.util.*;
public class Main {
    public static void main(String[] args) {
        Woman mother = new Woman("Rahima", "Ahmadov", 2000);
        Man father = new Man("Neriman", "Ahmadov", 2001);
        father.repairCar();
        mother.makeUp();

        Family familyAhamdov = new Family(mother, father);
        mother.setFamily(familyAhamdov);
        father.setFamily(familyAhamdov);

        Woman esma = new Woman("Esma", "Ahmadov", 2020, 160);

        System.out.println(esma.monday.name());     esma.setSchedule("Monday", "go to courses; watch a film");
        System.out.println(esma.tuesday.name());    esma.setSchedule("Sunday", "do home work");
        System.out.println(esma.wednesday.name());  esma.setSchedule("Wednesday", "do workout");
        System.out.println(esma.thursday.name());   esma.setSchedule("Friday", "read e-mails");
        System.out.println(esma.friday.name());     esma.setSchedule("Saturday", "do shopping");
        System.out.println(esma.saturday.name());   esma.setSchedule("Thursday", "visit grandparents");
        System.out.println(esma.sunday.name());     esma.setSchedule("Tuesday", "do household");

        familyAhamdov.addChild(esma);
        esma.setFamily(familyAhamdov);

        Dog esmasPet = new Dog("Rex",
                5, 49, new String[]{"eat, drink, sleep"});
        familyAhamdov.setPet(esmasPet);
        esma.greetPet();
        esma.describePet();
        esmasPet.respond();
        esmasPet.eat();
        esmasPet.foul();

        System.out.println(esmasPet.toString());

        System.out.println(esma.toString());

        Man child1 = new Man();
        Woman child2 = new Woman();
        Man child3 = new Man();
        familyAhamdov.addChild(child1);
        familyAhamdov.addChild(child2);
        familyAhamdov.addChild(child3);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(2);
        familyAhamdov.countFamily();

        System.out.println();

        familyAhamdov.deleteChild(child3);
        familyAhamdov.countFamily();
    }
}